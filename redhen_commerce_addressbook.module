<?php

/**
 * @file
 * RedHen integration with the Commerce Addressbook module.
 */

define('REDHEN_COMMERCE_ADDRESSBOOK_PROFILE_TYPE', 'redhen_commerce_addressbook_profile_type');
define('REDHEN_COMMERCE_ADDRESSBOOK_REQUIRE_ADDRESS', 'redhen_commerce_addressbook_require_address');

/**
 * Implements hook_redhen_settings().
 *
 * @return array
 *   Associative array of redhen settings for redhen commerce addressbook.
 */
function redhen_commerce_addressbook_redhen_settings() {
  $profiles_info = entity_get_info('commerce_customer_profile');
  if ($profiles_info && !empty($profiles_info['bundles'])) {
    $options = array();
    foreach ($profiles_info['bundles'] as $bundle => $info) {
      $options[$bundle] = $info['label'];
    }
    return array(
      'redhen_commerce_addressbook_profile_type' => array(
        '#type' => 'select',
        '#options' => $options,
        '#title' => t('RedHen Commerce Addressbook Profile Type'),
        '#required' => TRUE,
        '#description' => t('Default type of address profile to create/edit on redhen contacts.'),
        '#default_value' => variable_get(REDHEN_COMMERCE_ADDRESSBOOK_PROFILE_TYPE, FALSE),
      ),
      'redhen_commerce_addressbook_require_address' => array(
        '#type' => 'checkbox',
        '#title' => t('Require address fields when editing contacts'),
        '#required' => FALSE,
        '#description' => t('By default, addresses in commerce profiles are generally required. To avoid applying this requirement when editing Redhen Contacts, uncheck this setting.'),
        '#default_value' => variable_get(REDHEN_COMMERCE_ADDRESSBOOK_REQUIRE_ADDRESS, TRUE),
      ),
    );
  }
}

/**
 * Implements hook_commerce_customer_profile_type_info_alter().
 */
function redhen_commerce_addressbook_commerce_customer_profile_type_info_alter(&$profile_types) {
  $default = variable_get(REDHEN_COMMERCE_ADDRESSBOOK_PROFILE_TYPE, FALSE);
  if ($default) {
    // Sort the default profile type to the front, so it displays by default
    // on the Addressbook tab.
    $sort = function($a, $b) {
      if ($a === variable_get(REDHEN_COMMERCE_ADDRESSBOOK_PROFILE_TYPE, FALSE)) {
        return -1;
      };
      return 0;
    };
    uksort($profile_types, $sort);
  }
}

/**
 * Implements hook_schema_alter().
 */
function redhen_commerce_addressbook_schema_alter(&$schema) {
  $schema['commerce_customer_profile']['fields']['redhen_contact_id'] = array(
    'description' => 'The id of the redhen contact connected to this profile.',
    'type' => 'int',
    'not null' => FALSE,
    'default' => NULL,
  );
}

/**
 * Implements hook_entity_property_info_alter().
 */
function redhen_commerce_addressbook_entity_property_info_alter(&$info) {
  $info['commerce_customer_profile']['properties']['redhen_contact_id'] = array(
    'label' => t('RedHen Contact ID'),
    'type' => 'integer',
    'description' => t("The unique ID of the RedHen Contact the customer profile belongs to."),
    'setter callback' => 'entity_property_verbatim_set',
    'clear' => array('redhen_contact'),
    'schema field' => 'redhen_contact_id',
  );
  $info['commerce_customer_profile']['properties']['redhen_contact'] = array(
    'label' => t('RedHen Contact'),
    'type' => 'redhen_contact',
    'description' => t('Redhen Contact this profile belongs to.'),
    'getter callback' => 'redhen_commerce_addressbook_contact_get',
    'setter callback' => 'redhen_commerce_addressbook_contact_set',
    'required' => FALSE,
    'computed' => TRUE,
    'clear' => array('redhen_contact_id'),
  );
}

/**
 * Callback to get redhen contact for a custome profile.
 */
function redhen_commerce_addressbook_contact_get($profile, $options, $property_name, $entity_type) {
  if (isset($profile->redhen_contact_id)) {
    $contact = redhen_contact_load($profile->redhen_contact_id);
    if ($contact) {
      return $contact;
    }
  }
  return NULL;
}

/**
 * Callback to set $event->registration_capacity.
 */
function redhen_commerce_addressbook_contact_set(&$profile, $name, $value, $lang, $type, $info) {
  $profile->redhen_contact_id = $value;
}

/**
 * Implements hook_views_api().
 */
function redhen_commerce_addressbook_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'redhen_commerce_addressbook') . '/includes/views',
  );
}

/**
 * Implements hook_entity_insert().
 */
function redhen_commerce_addressbook_entity_insert($entity, $type) {
  // When a new profile is created for a user, and that user has a Contact
  // already, we link the profile to that Contact.
  if ($type == 'commerce_customer_profile' && !empty($entity->uid)) {
    $user = user_load($entity->uid);
    if ($contact = redhen_contact_load_by_user($user)) {
      $wrapper = entity_metadata_wrapper('commerce_customer_profile', $entity);
      $wrapper->redhen_contact = $contact;
      $wrapper->save();
    }
  }
  if ($type == 'redhen_donation' && $entity->author_uid == 0) {
    // Anonymous Redhen Donation: tie the Contact to the customer profile, since
    // we can't use the connection to the user to do so.
    $donation_wrapper = entity_metadata_wrapper('redhen_donation', $entity);
    $donation_wrapper->order->commerce_customer_billing->redhen_contact = $donation_wrapper->contact->value();
    $donation_wrapper->order->commerce_customer_billing->save();
  }
}

/**
 * Implements hook_entity_update().
 */
function redhen_commerce_addressbook_entity_update($entity, $type) {
  // Fix the contact's default address uid to match the new uid, if changed.
  // Occurs when assigning a user to a contact via redhen/contact/*/view/user.
  if ($type == 'redhen_contact' && $entity->uid != $entity->original->uid) {
    $profiles = redhen_commerce_addressbook_load_profiles($entity);
    if (!empty($profiles)) {
      foreach ($profiles as $profile) {
        $profile->uid = $entity->uid;
        commerce_customer_profile_save($profile);
      }
    }
  }
}

/**
 * Implements hook_field_extra_fields().
 */
function redhen_commerce_addressbook_field_extra_fields() {
  $return = array();

  $entity_type = 'redhen_contact';
  $entity_info = entity_get_info($entity_type);
  foreach (array_keys($entity_info['bundles']) as $bundle) {
    $return[$entity_type][$bundle]['form']['redhen_commerce_addressbook_commerce_addressbook'] = array(
      'label' => t('Addressbook'),
      'weight' => 5,
    );

    $return[$entity_type][$bundle]['display']['redhen_commerce_addressbook_commerce_addressbook'] = array(
      'label' => t('Addressbook'),
      'weight' => 5,
    );
  }

  return $return;
}

/**
 * Implements hook_entity_view().
 */
function redhen_commerce_addressbook_entity_view($entity, $type, $view_mode, $langcode) {
  if ($type == 'redhen_contact') {
    $extrafields = field_extra_fields_get_display('redhen_contact', $entity->type, $view_mode);
    if (isset($extrafields['redhen_commerce_addressbook_commerce_addressbook'])
      && isset($extrafields['redhen_commerce_addressbook_commerce_addressbook']['visible'])
      && $extrafields['redhen_commerce_addressbook_commerce_addressbook']['visible']) {
      // Borrowed/tweaked from commerce_addressbook_profile_page:
      $content = array(
        '#theme' => 'field',
        '#title' => 'Address Book',
        '#items' => array(),
//        0 => array(
//          '#markup' => $output,
//        ),
      );
      $arguments = array($entity->contact_id);
      $defaults_view = commerce_addressbook_retrieve_view('redhen_commerce_addressbook_defaults', 'default', $arguments);
      $list_view = commerce_addressbook_retrieve_view('redhen_commerce_addressbook', 'default', $arguments);
      if (!empty($defaults_view->result)) {
        $content['#items']['default'] = $defaults_view->result;
        $content['default'] = array(
          '#markup' => '<div id="redhen-addressbook-default-title">'
          . $defaults_view->get_title()
          . '</div><div id="redhen-addressbook--default">'
          . $defaults_view->render()
          . '</div>',
        );
      }
      if (!empty($list_view->result)) {
        $content['#items']['list'] = $list_view->result;
        $content['list'] = array(
          '#markup' => '<div id="redhen-addressbook-list-title">'
          . $list_view->get_title()
          . '</div><div id="redhen-addressbook-list">'
          . $list_view->render()
          . '</div>',
        );
      }
      if (count($content['#items']) == 0) {
        $content['#items']['empty'] = 'empty';
        $content['empty'] = array(
          '#markup' => '<div class="redhen-addressbook-nodata">' . t('Your address book is currently empty'));
      }
      // End borrowed code.

      // Don't show the "Create a new address" link if there's no associated user.
      if (isset($entity->uid)) {
        $profile_type = variable_get(REDHEN_COMMERCE_ADDRESSBOOK_PROFILE_TYPE, 'billing');
        $content['#items']['new_link'] = $profile_type;
        $content['new_link'] = array(
          '#markup' => '<div class="redhen-addressbook-new">' .
          l(
            t('Add address'),
            "user/$entity->uid/addressbook/$profile_type/create",
            array(
              'attributes' => array('title' => t('Create a new address.')),
              'query' => array('destination' => $GLOBALS['_GET']['q']),
            )) .
          '</div>',
        );
      }
      $entity->content['redhen_commerce_addressbook_commerce_addressbook'] = $content;
    }
  }
  elseif ($type == 'commerce_customer_profile' && $view_mode == 'addressbook') {
    if (isset($entity->content['commerce_addressbook_options']['default'])) {
      $entity->content['commerce_addressbook_options']['default']['#options']['query'] = drupal_get_destination();
    }
    if (isset($entity->content['commerce_addressbook_options']['edit'])) {
      $entity->content['commerce_addressbook_options']['edit']['#options']['query'] = drupal_get_destination();
    }
    if (isset($entity->content['commerce_addressbook_options']['delete'])) {
      $entity->content['commerce_addressbook_options']['delete']['#options']['query'] = drupal_get_destination();
    }
  }
}

/**
 * Loads the default profile for a given Contact.
 */
function redhen_commerce_addressbook_load_default_profile($contact_id, $type = FALSE) {
  if (!$type) {
    $type = variable_get(REDHEN_COMMERCE_ADDRESSBOOK_PROFILE_TYPE, 'billing');
  }
  $query = db_select('commerce_customer_profile', 'ccp');
  $query->join('commerce_addressbook_defaults', 'cad', 'cad.profile_id = ccp.profile_id');
  $query->fields('ccp', array('profile_id'));
  $query->condition('redhen_contact_id', $contact_id);
  $query->condition('ccp.type', $type);
  $result = $query->execute();
  $record = $result->fetchObject();

  return ($record) ? entity_load_single('commerce_customer_profile', $record->profile_id) : NULL;
}

/**
 * Load all profiles for a given Contact.
 */
function redhen_commerce_addressbook_load_profiles($contact) {
  $ids = redhen_commerce_addressbook_profiles_ids($contact);
  if (!empty($ids)) {
    return commerce_customer_profile_load_multiple($ids);
  }
  return NULL;
}
/**
 * Load all profiles for a given Contact.
 */
function redhen_commerce_addressbook_profiles_ids($contact) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_customer_profile')
    ->propertyCondition('redhen_contact_id', $contact->contact_id, '=');
  $result = $query->execute();
  if (isset($result['commerce_customer_profile'])) {
    return array_keys($result['commerce_customer_profile']);
  }
  return array();
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function redhen_commerce_addressbook_form_redhen_contact_contact_form_alter(&$form, &$form_state) {
  $defaults = array();
  $profile_type = variable_get(REDHEN_COMMERCE_ADDRESSBOOK_PROFILE_TYPE, 'billing');
  if (!empty($form['#entity']->is_new)) {
    $default_profile = commerce_customer_profile_new($profile_type);
  }
  else {
    $existing = redhen_commerce_addressbook_load_default_profile($form['#entity']->contact_id);
    $default_profile = $existing ?: commerce_customer_profile_new($profile_type);
  }
  // Commerce and Redhen Contact both use $form_state['build_info']['files']['form'].
  if (!empty($form_state['build_info']['files']['form'])) {
    $form_state['build_info']['files']['redhen_contact_form'] = $form_state['build_info']['files']['form'];
  }
  module_load_include('inc', 'commerce_customer', 'includes/commerce_customer_profile.forms');
  $form['redhen_commerce_addressbook_commerce_addressbook'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Address'),
    'form' => commerce_customer_customer_profile_form(array(), $form_state, $default_profile),
  );
  // Manage "required" overrides:
  if (!variable_get(REDHEN_COMMERCE_ADDRESSBOOK_REQUIRE_ADDRESS, TRUE)) {
    $defaults['#required'] = FALSE;
  }
  if (!empty($defaults)) {
    _redhen_commerce_addressbook_addressfield_settings_override($form['redhen_commerce_addressbook_commerce_addressbook']['form']['commerce_customer_address'], $defaults);
  }
  // Unset the contact forms action.
  unset($form['redhen_commerce_addressbook_commerce_addressbook']['form']['actions']);
  // Hide the User & Status fields:
  $form['redhen_commerce_addressbook_commerce_addressbook']['form']['user']['#access'] = FALSE;
  $form['redhen_commerce_addressbook_commerce_addressbook']['form']['profile_status']['#access'] = FALSE;
  // Add a submit handler for saving the profile.
  $form_state['redhen_commerce_addressbook_profile'] = $default_profile;
  $form['actions']['submit']['#submit'][] = 'redhen_commerce_addressbook_submit_contact_form';
}

/**
 * Recursive helper function to apply settings overrides for address fields.
 */
function _redhen_commerce_addressbook_addressfield_settings_override(&$form_item, $settings) {
  $elements = element_get_visible_children($form_item);
  foreach ($elements as $element) {
    if (is_array($form_item[$element])) {
      foreach ($settings as $setting => $value) {
        if (array_key_exists($setting, $form_item[$element])) {
          $form_item[$element][$setting] = $value;
        }
        if ($setting == '#required' && $value == FALSE && $element === 'country') {
          $form_item[$element]['#options'][''] = '-- select --';
        }
      }
      _redhen_commerce_addressbook_addressfield_settings_override($form_item[$element], $settings);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Also alter contact forms when displayed as embedded in User forms. Note that
 * this is necessary not only because the contact form does not itself get
 * altered when constructed by redhen_contact_form_user_register_form_alter(),
 * but because the location of the submit handler within the form state would
 * become ambiguous.
 */
function redhen_commerce_addressbook_form_user_register_form_alter(&$form, &$form_state) {
  // Check if embedding on user forms is enabled.
  if (variable_get(REDHEN_CONTACT_REG, FALSE)) {
    redhen_commerce_addressbook_form_redhen_contact_contact_form_alter($form['redhen_contact']['form'], $form_state);
    $form['#submit'][] = 'redhen_commerce_addressbook_submit_contact_form';
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function redhen_commerce_addressbook_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_alter') {
    redhen_commerce_addressbook_reorder_indexes($implementations, 'redhen_contact', 'redhen_commerce_addressbook');
  }
}

/**
 * Helper function to re-order hook implementations.
 */
function redhen_commerce_addressbook_reorder_indexes(&$array, $first, $second) {
  unset($array[$second]);
  $indexed = array_keys($array);
  $place = array_search($first, $indexed);
  array_splice($indexed, $place + 1, 0, $second);
  $array = array_combine($indexed, array_fill(0, count($indexed), FALSE));
}

/**
 * Processing function to handle embedded commerce profiles on contacts.
 *
 * See redhen_commerce_addressbook_form_redhen_contact_contact_form_alter().
 */
function redhen_commerce_addressbook_submit_contact_form($form, &$form_state) {
  // The form is located in different places depending on whether this is an
  // altered User form or an alterec Contact form.
  if ($form['#form_id'] == 'user_register_form') {
    $rc_form = &$form['redhen_contact']['form'];
  }
  else {
    $rc_form = $form;
  }
  if (!empty($form_state['values']['commerce_customer_address'])) {
    $lang = $rc_form['redhen_commerce_addressbook_commerce_addressbook']['form']['commerce_customer_address']['#language'];
    if (!empty($rc_form['redhen_commerce_addressbook_commerce_addressbook']['form']['#entity']->is_new)) {
      $existing_value = $rc_form['redhen_commerce_addressbook_commerce_addressbook']['form']['commerce_customer_address'][$lang][0]['#address'];
      if ($form_state['values']['commerce_customer_address'][$lang][0]) {
        $changes = array_diff($form_state['values']['commerce_customer_address'][$lang][0], $existing_value);
        if (count($changes) === 0) {
          // Empty address input, just the defaults. Don't add an address with
          // nothing but a country field.
          return;
        }
      }
    }
    $contact_wrapper = entity_metadata_wrapper('redhen_contact', $form_state['redhen_contact']);
    // Attach profile to Contact if necessary:
    if (empty($form_state['redhen_commerce_addressbook_profile']->redhen_contact_id)) {
      $form_state['redhen_commerce_addressbook_profile']->redhen_contact_id = $form_state['redhen_contact']->contact_id;
    }
    if ($contact_wrapper->user->value()) {
      // Attach profile to User:
      if (empty($form_state['redhen_commerce_addressbook_profile']->uid)) {
        $form_state['redhen_commerce_addressbook_profile']->uid = $contact_wrapper->user->getIdentifier();
      }
      // Check for any existing profiles on the Contact that should be tied to
      // the User:
      if ($existing = redhen_commerce_addressbook_load_profiles($form_state['redhen_contact'])) {
        foreach ($existing as $prof) {
          if (!$prof->uid) {
            $prof->uid = $contact_wrapper->user->getIdentifier();
            commerce_customer_profile_save($prof);
          }
        }
      }
    }
    $address_form = $rc_form['redhen_commerce_addressbook_commerce_addressbook']['form'];
    // We use a redhen contact function here because our alter/submit structure
    // is patterned after redhen_contact_form_user_register_form_alter(). This
    // function is abstract enough to fit our needs.
    $limited_state = redhen_contact_user_registration_form_state($address_form, $form_state);
    field_attach_submit('commerce_customer_profile', $form_state['redhen_commerce_addressbook_profile'], $address_form, $limited_state);
    commerce_customer_profile_save($form_state['redhen_commerce_addressbook_profile']);
    commerce_addressbook_set_default_profile($form_state['redhen_commerce_addressbook_profile']);
  }
}

/**
 * Implements hook_entity_delete().
 *
 * When a Contact sans User is deleted, delete any address profiles attached to
 * it as well.
 */
function redhen_commerce_addressbook_entity_delete($entity, $type) {
  if ($type == 'redhen_contact') {
    $wrapper = entity_metadata_wrapper('redhen_contact', $entity);
    if (!$wrapper->user->value()) {
      $profile_ids = redhen_commerce_addressbook_profiles_ids($entity);
      commerce_customer_profile_delete_multiple($profile_ids);
    }
  }
}

/**
 * Implements hook_query_TAG_alter().
 *
 * The commerce_entity_access_query_alter() function restricts access to the
 * commerce_customer_profile table based on the assumption you can only access
 * your own profile, or you have access to view any profile.
 *
 * Since we're attaching a part of the commerce customer profile (the address)
 * to the redhen contact here, we need to unset that access check. We only care
 * about whether the user can access the contact.
 *
 * This alter is supposed to only run with queries tagged with
 * redhen_contact_access i.e. the queries run from a redhen_contact page or
 * entity.
 *
 * @see commerce_entity_access_query_alter()
 * @see commerce_customer_query_commerce_customer_profile_access_alter()
 */
function redhen_commerce_addressbook_query_redhen_contact_access_alter(QueryAlterableInterface $query) {
  if ($query->hasTag('commerce_customer_profile_access')) {
    $where =& $query->conditions();
    foreach($where as $key => $condition) {
      // Find the condition added by commerce_entity_access_query_alter() and unset it.
      if (
        isset($condition['field'])
        && isset($condition['field']->conditions()[1]['field'])
        && is_string($condition['field']->conditions()[1]['field'])
        && $condition['field']->conditions()[1]['field'] == 'commerce_customer_profile.uid'
      ) {
        unset($where[$key]);
        break;
      }
    }
  }
}
