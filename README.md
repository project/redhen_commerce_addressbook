# Redhen Commerce Addressbook

Redhen Commerce Addressbook is a small module built to simplify address
management on sights using both Redhen and Drupal Commerce. It leverages the
Commerce Addressbook module to display and allow manipulation of a User's
Commerce Profiles from the Contact that is linked to that user.

Note that commerce_addressbook commit 7dd5ab8 is the recommended version as of
May 2016: that is currently the most recent commit, and the prior stable release
has some UX issues.