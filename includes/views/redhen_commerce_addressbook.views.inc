<?php

/**
 * @file
 * Views hooks for redhen_commerce_addressbook.
 */

/**
 * Implements hook_views_data_alter().
 */
function redhen_commerce_addressbook_views_data_alter(&$data) {
  if (isset($data['commerce_customer_profile'])) {
    $data['commerce_customer_profile']['redhen_contact_id'] = array(
      'title' => t('Contact'),
      'help' => t('Relate a profile to the contact it belongs to.'),
      'relationship' => array(
        'handler' => 'views_handler_relationship',
        'base' => 'redhen_contact',
        'field' => 'redhen_contact_id',
        'label' => t('Profile Contact'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    );
  }
}
